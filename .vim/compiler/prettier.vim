if executable('prettier')
    let &l:formatprg = 'prettier --option --option ...'
endif
